#Project 2
Wenjie Yao(wyao48), Haoming Jiang(hjiang98)
Since Wenjie Yao's laptop is MacBook, which has bad support for hpcviewer. So most work are done in Haoming's PC. Thus most of commits come from Haoming.

---

commit: fc75c0aada4d366afe707eb0fc5a16698217071b

Description: Initial commit. Haoming Jiang.

Running Environment: TACC Stampede2

Script:
```
#!/bin/sh
#SBATCH  -J proj2                        # Job name
#SBATCH  -p development                  # Queue (development or normal)
#SBATCH  -N 4                            # Number of nodes
#SBATCH --tasks-per-node 64              # Number of tasks per node
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # Our allocation
#SBATCH  -o proj2-%j.out                 # Standard output and error log

git rev-parse HEAD

git diff-files

module load hpctoolkit
make test_proj2

pwd; hostname; date

ibrun tacc_affinity hpcrun -t test_proj2 80 100000000 64 0 5

date
```

Output:
```
[0] test_proj2 minKeys 80 maxKeys 100000000 mult 64 seed 0
[0] Testing numKeysLocal 80, numKeysGlobal 20480, total bytes 163840
[0] Tested numKeysLocal 80, numKeysGlobal 20480, total bytes 163840:
average bandwidth 1.099968e+07
[0] Testing numKeysLocal 5120, numKeysGlobal 1310720, total bytes 10485760
[0] Tested numKeysLocal 5120, numKeysGlobal 1310720, total bytes 10485760:
average bandwidth 2.537136e+08
[0] Testing numKeysLocal 327680, numKeysGlobal 83886080, total bytes 671088640
[0] Tested numKeysLocal 327680, numKeysGlobal 83886080, total bytes 671088640:
average bandwidth 1.134023e+09
[0] Testing numKeysLocal 20971520, numKeysGlobal 5368709120, total bytes 42949672960
[0] Tested numKeysLocal 20971520, numKeysGlobal 5368709120, total bytes 42949672960:
average bandwidth 8.583383e+08
[0] Harmonic average bandwidth: 4.127964e+07
```
Profiling: ![p1](./figures/Screenshot from 2017-10-27 19-15-50.png)
Changes to make: Since lots of time waste in waiting. We want to use Isend in `uint64_swap_bitonic` to see if we can get improvement

---

commit: ce8bf26caf5edce7c9b09b3d176edbedc28cf0d7

Description: use Isend in `uint64_swap_bitonic`. However the program still spend ~10% of time on `uint64_swap_bitonic`.

Running Environment: TACC Stampede2

Script: The same as previous one

Output:
```
[0] test_proj2 minKeys 80 maxKeys 100000000 mult 64 seed 0
[0] Testing numKeysLocal 80, numKeysGlobal 20480, total bytes 163840
[0] Tested numKeysLocal 80, numKeysGlobal 20480, total bytes 163840:
average bandwidth 1.408609e+08
[0] Testing numKeysLocal 5120, numKeysGlobal 1310720, total bytes 10485760
[0] Tested numKeysLocal 5120, numKeysGlobal 1310720, total bytes 10485760:
average bandwidth 2.646153e+08
[0] Testing numKeysLocal 327680, numKeysGlobal 83886080, total bytes 671088640
[0] Tested numKeysLocal 327680, numKeysGlobal 83886080, total bytes 671088640:
average bandwidth 1.146163e+09
[0] Testing numKeysLocal 20971520, numKeysGlobal 5368709120, total bytes 42949672960
[0] Tested numKeysLocal 20971520, numKeysGlobal 5368709120, total bytes 42949672960:
average bandwidth 8.564349e+08
[0] Harmonic average bandwidth: 3.096364e+08
```
Profiling:![p2](./figures/Screenshot from 2017-10-27 15-00-35.png)

Changes to make: From profiling, we can know the time spends in bitonic:47-51 is about 10%. We want to reduce that by simplify the code.

---

commit: 533b3325134783cb8647c08cf323f1bd31d869aa

Description: We simplify the code. And reduce the total time cost of the section metioned above to about 8%. We can see that from the profiling.

Running Environment: TACC Stampede2

Script: Compared to the above script. We only do 3 times for each experiment.
```
#!/bin/sh
#SBATCH  -J proj2                        # Job name
#SBATCH  -p development                  # Queue (development or normal)
#SBATCH  -N 4                            # Number of nodes
#SBATCH --tasks-per-node 64              # Number of tasks per node
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # Our allocation
#SBATCH  -o proj2-%j.out                 # Standard output and error log

git rev-parse HEAD

git diff-files

module load hpctoolkit
make test_proj2

pwd; hostname; date

ibrun tacc_affinity hpcrun -t test_proj2 80 100000000 64 0 3

date
```

Output:
```
[0] test_proj2 minKeys 80 maxKeys 100000000 mult 64 seed 0
[0] Testing numKeysLocal 80, numKeysGlobal 20480, total bytes 163840
[0] Tested numKeysLocal 80, numKeysGlobal 20480, total bytes 163840:
average bandwidth 6.803706e+06
[0] Testing numKeysLocal 5120, numKeysGlobal 1310720, total bytes 10485760
[0] Tested numKeysLocal 5120, numKeysGlobal 1310720, total bytes 10485760:
average bandwidth 2.340366e+08
[0] Testing numKeysLocal 327680, numKeysGlobal 83886080, total bytes 671088640
[0] Tested numKeysLocal 327680, numKeysGlobal 83886080, total bytes 671088640:
average bandwidth 1.197278e+09
[0] Testing numKeysLocal 20971520, numKeysGlobal 5368709120, total bytes 42949672960
[0] Tested numKeysLocal 20971520, numKeysGlobal 5368709120, total bytes 42949672960:
average bandwidth 8.920539e+08
[0] Harmonic average bandwidth: 2.610833e+07
```
Profiling:![p3](./figures/Screenshot from 2017-10-27 23-42-55.png)

---

commit: 89dffa32787bc07d917c4070d8c2ec3fc84cf039

Description: We ran a new experiment to fing out the bottleneck of the algorithm.

Running Environment: TACC Stampede2

Script:
```
#!/bin/sh
#SBATCH  -J proj2                        # Job name
#SBATCH  -p development                  # Queue (development or normal)
#SBATCH  -N 8                            # Number of nodes
#SBATCH --tasks-per-node 64              # Number of tasks per node
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # Our allocation
#SBATCH  -o proj2-%j.out                 # Standard output and error log

git rev-parse HEAD

git diff-files

module load hpctoolkit
make test_proj2

pwd; hostname; date

ibrun tacc_affinity hpcrun -t test_proj2 100000 100000 64 0 3

date
```

Output:
```
[0] test_proj2 minKeys 100000 maxKeys 100000 mult 64 seed 0
[0] Testing numKeysLocal 100000, numKeysGlobal 51200000, total bytes 409600000
[0] Tested numKeysLocal 100000, numKeysGlobal 51200000, total bytes 409600000:
average bandwidth 1.559714e+09
[0] Harmonic average bandwidth: 1.559714e+09
```
Profiling:![p3](./figures/Screenshot from 2017-10-28 00-18-08.png)

Changes to make:
This is try to figure out where the program spend most of time. N = 8 means level = 9, which leads to 9 times call `uint64_swap_bitonic` with level 0. In the figure, we can see when swap happens in the pink part. And the green part is mainly use `uint64_sort_bitonic` to do local bitonic sort. From the light pink part, we can see where thread waiting a lot for others.

Therefore, progression may be make from 3 aspect.
1. Speed up local sort, may be use other sort methods. In the project, we modify the local bitonic code to save time from using less temporary variables.
2. Try to use less swap operations, which is not easy if we persist on using bitonic sort.
3. Try to reduce the waiting time. We use `MPI_Isend` here.

Notice that hpctraceviewer do not properly record the recurrence level of the program. It will not show more than 3 level of `uint64_sort_bitonic`, which actually is 10 for the max.

---

commit: 61f35fa34e46d1f40dd8b20d7ba6392b59b084e5

Description: Run `slurm-proj2-stampede2-final.sh`

Running Environment: TACC Stampede2

Output:
```
[0] test_proj2 minKeys 80 maxKeys 100000000 mult 64 seed 0
[0] Testing numKeysLocal 80, numKeysGlobal 20480, total bytes 163840
[0] Tested numKeysLocal 80, numKeysGlobal 20480, total bytes 163840:
average bandwidth 3.459932e+08
[0] Testing numKeysLocal 5120, numKeysGlobal 1310720, total bytes 10485760
[0] Tested numKeysLocal 5120, numKeysGlobal 1310720, total bytes 10485760:
average bandwidth 1.325232e+09
[0] Testing numKeysLocal 327680, numKeysGlobal 83886080, total bytes 671088640
[0] Tested numKeysLocal 327680, numKeysGlobal 83886080, total bytes 671088640:
average bandwidth 1.302226e+09
[0] Testing numKeysLocal 20971520, numKeysGlobal 5368709120, total bytes 42949672960
[0] Tested numKeysLocal 20971520, numKeysGlobal 5368709120, total bytes 42949672960:
average bandwidth 9.404802e+08
[0] Harmonic average bandwidth: 7.304577e+08
```
